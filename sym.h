/*
 * sym.h
 *
 *  Created on: Apr 5, 2021
 *      Author: vlad
 */

#ifndef SYM_H_
#define SYM_H_


void DBG_GetSymbol(const char *elffile,const char *symbolname,char* type, unsigned int *size, unsigned long int *address);



#endif /* SYM_H_ */
