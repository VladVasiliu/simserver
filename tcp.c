/*
 * tcp.c
 *
 *  Created on: Apr 11, 2021
 *      Author: vlad
 */

#include <asm-generic/errno-base.h>
#include <stdio.h>
#include <string.h> //strlen
#include <stdlib.h> //strlen
#include <sys/socket.h>
#include <arpa/inet.h> //inet_addr
#include <unistd.h>    //write
#include <pthread.h>   //for threading , link with lpthread
#include "SimServer.h"
#include <errno.h>
#include <poll.h>
#include <fcntl.h>
#define TCP_MAXBUFF (1024 * 2)
//the thread function

static pthread_t thread_id;
static int TCP_fd = 0;
static int TCP_StopServer = 0;

void TCP_ServerHandler(int *socket_desc);
void TCP_ConectionHandler(int sock);

void TCP_SetNonBlock(int fd)
{
    int flags = 0;
    /* ---- Make the Server Socket Non Blocking ---- */
    flags = fcntl(fd, F_GETFL, 0);
    fcntl(fd, F_SETFL, flags | O_NONBLOCK);
}

int TCP_Init(int port)
{
    int socket_desc;
    struct sockaddr_in server;

    /* ---- Create a socket---- */
    printf("INFO : Server : Create Socket\n");
    socket_desc = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_desc == -1)
    {
        fprintf(stderr, "ERROR : Server : Could not create socket\n");
        return -1;
    }

    /* ---- Prepare the sockaddr_in structure ---*/
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(port);

    //* ---- Make the bind ----- */
    if (bind(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0)
    {
        fprintf(stderr, "ERROR : Server : Bind failed. Server Cannot be created\n");
        return -1;
    }

    printf("INFO : Server : Listen on port %d (%d)\n", port, socket_desc);
    if (listen(socket_desc, 1) == 0)
    {
        TCP_fd = socket_desc;
        TCP_StopServer = 0;

        printf("INFO : Server : Waiting for incoming connections...\n");
        if (pthread_create(&thread_id, NULL, (void *(*)(void *)) & TCP_ServerHandler, (&TCP_fd)) < 0)
        {
            fprintf(stderr, "ERROR : Server : Error Creating Thread\n");
        }
    }
    else
    {
        fprintf(stderr, "ERROR : Server : listen not possible\n");
    }

    return 0;
}

int TCP_Accept(int fd, __SOCKADDR_ARG addr, socklen_t *__restrict addr_len)
{
    struct pollfd pfds;
    int ready;
    int ret = 0;

    pfds.fd = fd;
    pfds.events = POLLIN | POLLOUT | POLLHUP | POLLERR | POLLNVAL;
    pfds.revents = 0;

    ready = poll(&pfds, 1, 5000);

    if ((pfds.revents & (POLLIN | POLLOUT)) == 0)
    {
        ret = -1;
        fprintf(stderr, "ERROR : Server : Accept Timeout \n");
    }
    else
    {
        ret = accept(fd, addr, addr_len);

        if (ret < 0)
        {
            fprintf(stderr, "ERROR : Server : Accept failed\n");
        }
    }

    return ret;
}

ssize_t TCP_Receive(int fd, void *buf, size_t n, int flags)
{
    struct pollfd pfds;
    int ready;
    int ret = 0;

    if(n == 0)  
    {
        return 0;
    }


    pfds.fd = fd;
    pfds.events = POLLIN | POLLHUP | POLLERR | POLLNVAL;
    pfds.revents = 0;

    ready = poll(&pfds, 1, 5000);

    if ((pfds.revents & POLLIN) != 0)
    {
        ret = recv(fd, buf, n, flags);
    }
    else
    {
        ret = 0;
        fprintf(stderr, "ERROR : Server : Receive Timeout \n");
    }

    return ret;
}
int TCP_Interpreter(char *client_message, char *client_response)
{
    unsigned long DataAddress = 0;
    unsigned long DataTStamp = 0;
    unsigned long DataSize = 0;
    unsigned long data_write = 0;
    unsigned long Data = 0;
    char operation = '\n';
    int ret = 1;
    int el = 0;

    /* ---- Read Address and size information -----*/
    el = sscanf(client_message, "[%lX][%lX]%c%lX", &DataAddress, &DataSize, &operation, &data_write);

    if (el >= 2)
    { 
                
        if (operation == '=')
        {
            MAIN_SetData(DataAddress, DataSize, data_write);
        }

        Data = MAIN_GetData(&DataTStamp,DataAddress, DataSize); 
        /* ---- Send the message back to client ----*/
        sprintf(client_response, "[0x%lX][0x%lX][0x%lX]=0x%lX\n",DataTStamp, DataAddress, DataSize, Data);
        printf("INFO : [0x%lX][0x%lX][0x%lX]=0x%lX\n",DataTStamp, DataAddress, DataSize, Data);
    }
    else
    {
        sprintf(client_response, "ERROR : Wrong formated message\n");

        fprintf(stderr, "ERROR : Server : Wrong formated message (%s)(%d)\n", client_message, el);
    }

    return ret;
}

void TCP_ServerHandler(int *socket_desc)
{
    int client_sock = 0;
    int server_sock = *socket_desc;
    int c = sizeof(struct sockaddr_in);
    struct sockaddr_in client;

    MAIN_ServerHook();

    TCP_SetNonBlock(server_sock);

    while (TCP_StopServer == 0)
    {
        while ((client_sock = TCP_Accept(server_sock, (struct sockaddr *)&client, (socklen_t *)&c)) > 0)
        {
            TCP_SetNonBlock(client_sock);

            printf("INFO : Server : Connection accepted\n");

            TCP_ConectionHandler(client_sock);

            close(client_sock);

            if (TCP_StopServer != 0)
            {
                printf("INFO : Server : Shut Down Req Ack\n");
                break;
            }
        }
    }
    printf("INFO : Server : Performing Shut Down\n");
    printf("INFO : Server : Last Error : %s\n", strerror(errno));
    close(server_sock);
}

unsigned long TCP_FindAndHandleMsg(int fd,char *buff, unsigned long size)
{
    int ret = 0;
    unsigned long start = 0;
    unsigned long end = 0;
    unsigned long count = 0;
    char command[TCP_MAXBUFF];
    char client_response[TCP_MAXBUFF];
    enum {msg_start=0,msg_end,msg_found} msgstate = msg_start; 
    for (count = 0; count < size; count++)
    {

        switch(msgstate)
        {
            case msg_start : {start = count; msgstate = (buff[start] == '[' )? msg_end:msg_start;} break;
            case msg_end   : {end   = count; msgstate = (buff[end]   == '\n')? msg_found:msg_end;} break;
            default:{}break;
        }   

        if (msgstate == msg_found)
        {
            /* ---- Possible mesage was found -----*/
            memset(command, 0, sizeof(command));
            memcpy(command, &buff[start], end - start);

            /* ---- Initialize the response ----- */
            memset(client_response, 0, sizeof(client_response));
             

            if (TCP_Interpreter(command, client_response) > 0)
            {
                /* ---- Write the response ----*/
                write(fd, client_response, strlen(client_response));
            }
            /* ---- Reset State machine ----*/
            msgstate = msg_start;
        }
    }

    if(msgstate == msg_end)
    {
        /* ---- Shift the message buffer ----*/
        ret = size - start;
        memcpy(&buff[0], &buff[start], ret);
        memset(&buff[ret], 0, size - ret);
        
    }
    else 
    {
        memset(buff,0,size);
        ret = 0;
    } 
    return ret;
}
/*
 * This will handle connection for each client
 * */
void TCP_ConectionHandler(int sock)
{
    int read_size = 0;
    char client_message[TCP_MAXBUFF];
    unsigned long Data = 0;
    unsigned long DataAddress = 0;
    unsigned long DataSize = 0;
    static unsigned count = 0;

    memset(client_message,0,sizeof(client_message));

    /* ---- keep the server alive as long a client is connected ----*/
    do
    {

        /* ---- Receive a message from client ---- */
        if ((read_size = TCP_Receive(sock, &client_message[count], (TCP_MAXBUFF - count - 1), 0)) > 0)
        {
            count += read_size;
        }
        /* ----- Find a message in the receive buffer ----*/
        count = TCP_FindAndHandleMsg(sock,client_message, count);

    } while ((read_size > 0)|(count>0));

    printf("INFO : Server : Client disconnected (read_size = %d)\n", read_size);
    printf("INFO : Server : Last Error : %s\n", strerror(errno));
    fflush(stdout);
}
void TCP_Close(void)
{
    printf("INFO : Stop TCP Server\n");

    TCP_StopServer = 1;

    /* ---- Now join the thread , so that we dont terminate before the thread ---- */
    if (thread_id > 0)
    {
        pthread_join(thread_id, NULL);
    }
}