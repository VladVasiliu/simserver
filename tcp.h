/*
 * tcp.h
 *
 *  Created on: Apr 11, 2021
 *      Author: vlad
 */

#ifndef TCP_H_
#define TCP_H_


int TCP_Init(int port);

void TCP_Close(void);

#endif /* TCP_H_ */
