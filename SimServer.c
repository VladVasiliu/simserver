/*
 ============================================================================
 Name        : SimServer.c
 Author      : Vlad Vasiliu
 Version     :
 Copyright   : GNU2.0
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "dbg.h"
#include "tcp.h"

DBG_BreakPoint BP;
char elfpath[1024];
char symname[1024];
unsigned long Time=0;
enum
{
    MAIN_NoReq = 0,
    MAIN_Read,
    MAIN_ReadDone,
    MAIN_Write,
    MAIN_WriteDone
} MAIN_State = MAIN_NoReq;

unsigned long MAIN_GetData(unsigned long *Tstamp,unsigned long address, unsigned long size)
{
    unsigned long ret = 0;  
    DBG_WaitForBreakpoint(&BP);
    Time++;

    if(Tstamp != NULL)
    {
        *Tstamp = Time;
    }
    
    ret = DBG_GetData(address, size);
    DBG_Continue();
    return ret;
}

void MAIN_SetData(unsigned long address, unsigned long size, unsigned long data)
{ 
    DBG_WaitForBreakpoint(&BP);
    Time++;
    DBG_SetData(address, size, data);
    DBG_Continue();
}
void MAIN_ServerHook()
{
    pid_t pid;
    pid = fork();

    if (pid == 0)
    {
        /* ---- This is the child process -----*/
        DBG_StartTrace(0);
        execv(elfpath, NULL);
        printf("INFO : Error executing program (%d | %s)!\n", errno, strerror(errno));
        exit(1);
    }
    else
    {
        /* ---- This is the parent process ---*/
        int waitstatus = 0;
        wait(&waitstatus);
        if (WIFEXITED(waitstatus))
        {
            exit(-1);
        }
        else
        {
 
            printf("INFO : Process was initialized!(pid=%d)\n",pid);
            DBG_SetPid(pid);

            if(DBG_SetBreakpoint(elfpath,symname, &BP) !=0)
            {
                DBG_Continue();
            }

        }
    }
}
int main(int argc,char *argv[])
{
    

    if (argc < 3)
    {
        printf("INFO : Usage  arg0 - program to be attached\n");
        printf("INFO : Usage  arg1 - symbol for the breakpoint\n");
        return -1;
    }
    else
    {
        printf("INFO : Program Name : %s\n", argv[1]);
        printf("INFO : Symbol  Name : %s\n", argv[2]);

        strncpy(elfpath,argv[1],sizeof(elfpath));
        strncpy(symname,argv[2],sizeof(symname));
        
    }

    TCP_Init(0x9999);
    getchar();
    TCP_Close();

    return EXIT_SUCCESS;
}
