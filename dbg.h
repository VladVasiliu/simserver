/*
 * dbg.h
 *
 *  Created on: Apr 5, 2021
 *      Author: vlad
 */

#ifndef DBG_H_
#define DBG_H_
#include <sys/types.h>
typedef struct
{
	 char *Name;
	 unsigned long long address;
	 unsigned long long instruction;
	 pid_t pid;
}DBG_BreakPoint;

void  DBG_StartTrace();
void  DBG_SetPid(pid_t child);
int  DBG_SetBreakpoint(const char *elf,const char *functionname,DBG_BreakPoint *BrkPoint);
void  DBG_ResetBreakpoint(DBG_BreakPoint *BrkPoint);
void  DBG_WaitForBreakpoint(DBG_BreakPoint *BrkPoint);
void  DBG_Continue();
unsigned long  DBG_GetData(unsigned long  address,unsigned long size);
void DBG_SetData(unsigned long  address,unsigned long size,unsigned long data);
#endif /* DBG_H_ */
