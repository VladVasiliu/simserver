
#ifndef _SIMSERVER_
#define _SIMSERVER_
unsigned long MAIN_GetData(unsigned long *Tstamp,unsigned long address, unsigned long size);
void MAIN_SetData(unsigned long address, unsigned long size, unsigned long data);
void MAIN_ServerHook();
#endif
