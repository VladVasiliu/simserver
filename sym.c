/*
 * sym.c
 *
 *  Created on: Apr 5, 2021
 *      Author: vlad
 */

#include <stdio.h>
#include <string.h>

#define DBG_MAX_CMDLINE ((4ul*1024ul))
#define DBG_ProgramName "SymbolDecoder "
const char TypeTable[16][32] =
{
		"DW_ATE_UNKNOWN",
		"DW_ATE_address",
		"DW_ATE_boolean",
		"DW_ATE_complex_float",
		"DW_ATE_float",
		"DW_ATE_signed",
		"DW_ATE_signed_char",
		"DW_ATE_unsigned",
		"DW_ATE_unsigned_char",
		/* DWARF3/DWARF3f  */
		"DW_ATE_imaginary_floa",
		"DW_ATE_packed_decimal",
		"DW_ATE_numeric_string",
		"DW_ATE_edited",
		"DW_ATE_signed_fixed ",
		"DW_ATE_unsigned_fixed",
		"DW_ATE_decimal_float",
		/*DWARF4 ---- Not used ----*/
};
#define DBG_ADDRESS_LOW_IDENT "INFO : Address Low :"
#define DBG_ADDRESS_IDENT     "INFO : Address :"
#define DBG_TYPE_IDENT        "INFO : Type    :"
#define DBG_SIZE_IDENT        "INFO : Size    :"
#define DBG_ERROR_IDENT       "ERROR :"
#define DBG_DEBUG_IDENT       "DEBUG :"
void DBG_GetSymbol(const char *elffile,const char *symbolname,char* type, unsigned int *size, unsigned long int *address)
{
	char commandline[DBG_MAX_CMDLINE];
	FILE *out = NULL;
	char buffer[1024];

	/* ---- Clear working data ----*/
	memset(commandline,0,DBG_MAX_CMDLINE);
	/* ---- Create the command line ----*/
	strcat(commandline,DBG_ProgramName);
	strcat(commandline,elffile);
	strcat(commandline," ");
	strcat(commandline,symbolname);

	/* ----- Call the command -----*/
	//printf("INFO :%s\n",commandline);
	out =  popen(commandline,"r");

	if(out != NULL)
	{
		while (fgets(buffer, 1024, out) != NULL)
		{
			//printf("%s", buffer);
			if(memcmp(buffer,DBG_ADDRESS_IDENT,strlen(DBG_ADDRESS_IDENT))==0)
			{
				sscanf(&buffer[strlen(DBG_ADDRESS_IDENT)],"%lX",address);
			}
			if(memcmp(buffer,DBG_ADDRESS_LOW_IDENT,strlen(DBG_ADDRESS_LOW_IDENT))==0)
			{
				sscanf(&buffer[strlen(DBG_ADDRESS_LOW_IDENT)],"%lX",address);
			}

			if(memcmp(buffer,DBG_SIZE_IDENT,strlen(DBG_SIZE_IDENT))==0)
			{
				sscanf(&buffer[strlen(DBG_SIZE_IDENT)],"%d",size);
			}
			if(memcmp(buffer,DBG_TYPE_IDENT,strlen(DBG_TYPE_IDENT))==0)
			{
				sscanf(&buffer[strlen(DBG_TYPE_IDENT)],"%s",type);
			}

		}
		pclose(out);
	}
	else
	{
		printf("ERROR : Could not start the process %s\n",commandline);
	}
}
/* ---- End of Line ---- */

