#include "stdio.h"
#include "tcp.h"
#include <netdb.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <wait.h>
#include <unistd.h>
#include "SimServer.h"
#define MAX 255
#define PORT 9999

unsigned long DBG_GetData_Address = 0;
unsigned long DBG_GetData_Size = 0;
unsigned long DBG_GetData_Data = 0;
unsigned long DBG_GetData_Tstamp = 0;
unsigned long DBG_SetData_Address = 0;
unsigned long DBG_SetData_Size = 0;
unsigned long DBG_SetData_Data = 0;

void MAIN_ServerHook()
{
}
unsigned long MAIN_GetData(unsigned long *Tstamp,unsigned long address, unsigned long size)
{
    DBG_GetData_Address = address;
    DBG_GetData_Size = size;
    if(Tstamp != NULL)
    {
       *Tstamp = DBG_GetData_Tstamp;
    }
    return DBG_GetData_Data;
}

void MAIN_SetData(unsigned long address, unsigned long size, unsigned long data)
{
    DBG_SetData_Data = data;
    DBG_SetData_Address = address;
    DBG_SetData_Size = size;
}

void StartClient(void fn(int sockfd, unsigned long address, unsigned long size, unsigned long *data), unsigned long addr, unsigned long siz, unsigned long dat)
{
    int sockfd = -1, connfd = -1;
    struct sockaddr_in servaddr, cli;

    // socket create and varification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        fprintf(stderr, "ERROR :Client socket creation failed...\n");
        exit(-1);
    }

    printf("INFO :Client Socket successfully created..\n");
    bzero(&servaddr, sizeof(servaddr));

    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    servaddr.sin_port = htons(PORT);

    // connect the client socket to server socket
    if (connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) != 0)
    {
        fprintf(stderr, "ERROR :Client Connection with the server failed...\n");
        exit(-1);
    }

    printf("INFO :Client Connected to the server..\n");

    // function for chat
    if (fn != NULL)
    {
        fn(sockfd, addr, siz, &dat);
    }
    // close the socket
    close(sockfd);
}
int StartClientNoClose()
{
    int sockfd = -1, connfd = -1;
    struct sockaddr_in servaddr, cli;

    // socket create and varification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        fprintf(stderr, "ERROR :Client socket creation failed...\n");
        exit(-1);
    }

    printf("INFO :Client Socket successfully created..\n");
    bzero(&servaddr, sizeof(servaddr));

    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    servaddr.sin_port = htons(PORT);

    // connect the client socket to server socket
    if (connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) != 0)
    {
        fprintf(stderr, "ERROR : Client Connection with the server failed...\n");
        exit(-1);
    }

    printf("INFO : Client Connected to the server..\n");

    return (sockfd);
}

void TestCase_NormalCommunicationRx(int sockfd, unsigned long address, unsigned long size, unsigned long *data)
{
    char in[MAX];
    char out[MAX];
    unsigned long tsample_out = 0;
    unsigned long addr_out = 0;
    unsigned long size_out = 0;
    unsigned long data_out = 0;
    // Init test data
    DBG_GetData_Data = *data;
    printf("INFO : Expects %lX\n", DBG_GetData_Data);

    bzero(in, sizeof(in));
    bzero(out, sizeof(out));

    sprintf(in, "[0x%08lX][0x%04lX]\n", address, size);
    write(sockfd, in, strlen(in));
    read(sockfd, out, sizeof(out));
    printf("INFO : From Server : %s", out);
    if (sscanf(out, "[%lX][%lX][%lX]=%lX\n",&tsample_out, &addr_out, &size_out, &data_out) != 4)
    {
        fprintf(stderr, "ERROR : Unexpected response : %s\n", out);
        exit(-1);
    }
    if (address != addr_out)
    {
        fprintf(stderr, "ERROR : Unexpected response address (%lX!=%lX)\n", address, addr_out);
        exit(-1);
    }

    if (size != size_out)
    {
        fprintf(stderr, "ERROR : Unexpected response size (%lX!=%lX)\n", size, size_out);
        exit(-1);
    }
    if (DBG_GetData_Address != address)
    {
        fprintf(stderr, "ERROR : Unexpected Address (%lX != %lX)\n", DBG_GetData_Address, address);
        exit(-1);
    }
    if (DBG_GetData_Size != size)
    {
        fprintf(stderr, "ERROR : Unexpected Size (%lX != %lX)\n", DBG_GetData_Size, size);
        exit(-1);
    }
    if (DBG_GetData_Data != data_out)
    {
        fprintf(stderr, "ERROR : Unexpected data (%lX != %lX)\n", DBG_GetData_Data, data_out);
        exit(-1);
    }
    if (DBG_GetData_Data != *data)
    {
        fprintf(stderr, "ERROR : Unexpected data (%lX != %lX)\n", DBG_GetData_Data, *data);
        exit(-1);
    }
}

void TestCase_NormalCommunicationTx(int sockfd, unsigned long address, unsigned long size, unsigned long *data)
{
    char in[MAX];
    char out[MAX];
    unsigned long addr_out = 0;
    unsigned long size_out = 0;
    unsigned long data_out = 0;
    unsigned long tsample_out = 0;
    // Init test data
    DBG_SetData_Data = 0;
    DBG_GetData_Data = *data;
    printf("INFO : Expects %lX\n", DBG_GetData_Data);
    bzero(in, sizeof(in));
    bzero(out, sizeof(out));

    sprintf(in, "[0x%08lX][0x%04lX]=0x%lX\n", address, size, *data);
    write(sockfd, in, strlen(in)); 
    read(sockfd, out, sizeof(out));
    printf("INFO : From Server : %s\n", out);
    if (sscanf(out, "[%lX][%lX][%lX]=%lX\n",&tsample_out, &addr_out, &size_out, &data_out) != 4)
    {
        fprintf(stderr, "ERROR : Unexpected response : %s\n", out);
        exit(-1);
    }
    if (address != addr_out)
    {
        fprintf(stderr, "ERROR : Unexpected response address (%lX!=%lX)\n", address, addr_out);
        exit(-1);
    }

    if (size != size_out)
    {
        fprintf(stderr, "ERROR : Unexpected response size (%lX!=%lX)\n", size, size_out);
        exit(-1);
    }
    if (DBG_SetData_Address != address)
    {
        fprintf(stderr, "ERROR : Unexpected Address (%lX != %lX)\n", DBG_SetData_Address, address);
        exit(-1);
    }
    if (DBG_SetData_Size != size)
    {
        fprintf(stderr, "ERROR : Unexpected Size (%lX != %lX)\n", DBG_SetData_Size, size);
        exit(-1);
    }
    if (DBG_SetData_Data != data_out)
    {
        fprintf(stderr, "ERROR : Unexpected data1 (%lX != %lX)\n", DBG_SetData_Data, data_out);
        exit(-1);
    }
    if (DBG_SetData_Data != *data)
    {
        fprintf(stderr, "ERROR : Unexpected data2 (%lX != %lX)\n", DBG_SetData_Data, *data);
        exit(-1);
    }
}


void TestCase_IncompleteCommunicationTxUnaligned(int sockfd, unsigned long address, unsigned long size, unsigned long *data)
{
  char in[MAX];
    char out[MAX];
    unsigned long addr_out = 0;
    unsigned long size_out = 0;
    unsigned long data_out = 0;
    unsigned long tsample_out = 0;
    // Init test data
    DBG_SetData_Data = 0;
    DBG_GetData_Data = *data;
    printf("INFO : Expects %lX\n", DBG_GetData_Data);
    bzero(in, sizeof(in));
    bzero(out, sizeof(out));

    sprintf(in, "jhfp68rpyv89760990[0x%08lX][0x%04lX]=0x%lX\n", address, size, *data);
    write(sockfd, in, strlen(in)); 
    read(sockfd, out, sizeof(out));
    printf("INFO : From Server : %s\n", out);
    if (sscanf(out, "[%lX][%lX][%lX]=%lX\n",&tsample_out, &addr_out, &size_out, &data_out) != 4)
    {
        fprintf(stderr, "ERROR : Unexpected response : %s\n", out);
        exit(-1);
    }
    if (address != addr_out)
    {
        fprintf(stderr, "ERROR : Unexpected response address (%lX!=%lX)\n", address, addr_out);
        exit(-1);
    }

    if (size != size_out)
    {
        fprintf(stderr, "ERROR : Unexpected response size (%lX!=%lX)\n", size, size_out);
        exit(-1);
    }
    if (DBG_SetData_Address != address)
    {
        fprintf(stderr, "ERROR : Unexpected Address (%lX != %lX)\n", DBG_SetData_Address, address);
        exit(-1);
    }
    if (DBG_SetData_Size != size)
    {
        fprintf(stderr, "ERROR : Unexpected Size (%lX != %lX)\n", DBG_SetData_Size, size);
        exit(-1);
    }
    if (DBG_SetData_Data != data_out)
    {
        fprintf(stderr, "ERROR : Unexpected data1 (%lX != %lX)\n", DBG_SetData_Data, data_out);
        exit(-1);
    }
    if (DBG_SetData_Data != *data)
    {
        fprintf(stderr, "ERROR : Unexpected data2 (%lX != %lX)\n", DBG_SetData_Data, *data);
        exit(-1);
    }
}
void TestCase_IncompleteCommunicationTxIncomplete(int sockfd, unsigned long address, unsigned long size, unsigned long *data)
{
    char in[MAX];
    char out[MAX];
    unsigned long addr_out = 0;
    unsigned long size_out = 0;
    unsigned long data_out = 0;
    unsigned long tsample_out = 0;
    // Init test data
    DBG_SetData_Data = 0;
    DBG_GetData_Data = *data;
    printf("INFO : Expects %lX\n", DBG_GetData_Data);
    bzero(in, sizeof(in));
    bzero(out, sizeof(out));

    sprintf(in, "[0x[0x%04lX]=0x%lX\n", size, *data);
    write(sockfd, in, strlen(in)); 
    read(sockfd, out, sizeof(out));
    printf("INFO : From Server : %s\n", out);
   /* if (sscanf(out, "[%lX][%lX]=%lX\n", &addr_out, &size_out, &data_out) == 3)
    {
        fprintf(stderr, "ERROR : Unexpected response : %s\n", out);
        exit(-1);
    }*/

}

int main(int argc, char *argv[])
{
    TCP_Init(PORT);
    sleep(1);
    StartClient(&TestCase_NormalCommunicationRx, 0x55, 1, 0x44);
    StartClient(&TestCase_NormalCommunicationRx, 0xCAFE, 2, 0xBEEF);
    StartClient(&TestCase_NormalCommunicationRx, 0xAACAFE, 3, 0xBEEFEE);
    StartClient(&TestCase_NormalCommunicationRx, 0xCAFEDEAD, 4, 0xDEADBEEF);
    StartClient(&TestCase_NormalCommunicationRx, 0xCAFEDEAD55AA, 5, 0xDEADBEEF12);
    StartClient(&TestCase_NormalCommunicationRx, 0xCAFEDEADAA55, 6, 0xDEADBEEF3421);
    StartClient(&TestCase_NormalCommunicationRx, 0xCAFEDEAD1234, 7, 0xDEADBEEF5A5A5A);
    StartClient(&TestCase_NormalCommunicationRx, 0xCAFEDEADBEAFCAFE, 8, 0xCAFEBEEF55AA55AA);
    StartClient(&TestCase_NormalCommunicationTx, 0x55, 1, 0x44);
    StartClient(&TestCase_NormalCommunicationTx, 0xCAFE, 2, 0xBEEF);
    StartClient(&TestCase_NormalCommunicationTx, 0xAACAFE, 3, 0xBEEFEE);
    StartClient(&TestCase_NormalCommunicationTx, 0xCAFEDEAD, 4, 0xDEADBEEF);
    StartClient(&TestCase_NormalCommunicationTx, 0xCAFEDEAD55AA, 5, 0xDEADBEEF12);
    StartClient(&TestCase_NormalCommunicationTx, 0xCAFEDEADAA55, 6, 0xDEADBEEF3421);
    StartClient(&TestCase_NormalCommunicationTx, 0xCAFEDEAD1234, 7, 0xDEADBEEF5A5A5A);
    StartClient(&TestCase_NormalCommunicationTx, 0xCAFEDEADBEAFCAFE, 8, 0xCAFEBEEF55AA55AA);
    int sockfd = StartClientNoClose();

    unsigned long data = 0x44;
    TestCase_NormalCommunicationRx(sockfd, 0x55, 1, &data);
    data = 0xBEEF;
    TestCase_NormalCommunicationRx(sockfd, 0xCAFE, 2, &data);
    data = 0xBEEFEE;
    TestCase_NormalCommunicationRx(sockfd, 0xAACAFE, 3, &data);
    data = 0xDEADBEEF;
    TestCase_NormalCommunicationRx(sockfd, 0xCAFEDEAD, 4, &data);
    data = 0xDEADBEEF12;
    TestCase_NormalCommunicationRx(sockfd, 0xCAFEDEAD55AA, 5, &data);
    data = 0xDEADBEEF3421;
    TestCase_NormalCommunicationRx(sockfd, 0xCAFEDEADAA55, 6, &data);
    data = 0xDEADBEEF5A5A5A;
    TestCase_NormalCommunicationRx(sockfd, 0xCAFEDEAD1234, 7, &data);
    data = 0xCAFEBEEF55AA55AA;
    TestCase_NormalCommunicationRx(sockfd, 0xCAFEDEADBEAFCAFE, 8, &data);
    data = 0x44;
    TestCase_NormalCommunicationTx(sockfd, 0x55, 1, &data);
    data = 0xBEEF;
    TestCase_NormalCommunicationTx(sockfd, 0xCAFE, 2, &data);
    data = 0xBEEFEE;
    TestCase_NormalCommunicationTx(sockfd, 0xAACAFE, 3, &data);
    data = 0xDEADBEEF;
    TestCase_NormalCommunicationTx(sockfd, 0xCAFEDEAD, 4, &data);
    data = 0xDEADBEEF12;
    TestCase_NormalCommunicationTx(sockfd, 0xCAFEDEAD55AA, 5, &data);
    data = 0xDEADBEEF3421;
    TestCase_NormalCommunicationTx(sockfd, 0xCAFEDEADAA55, 6, &data);
    data = 0xDEADBEEF5A5A5A;
    TestCase_NormalCommunicationTx(sockfd, 0xCAFEDEAD1234, 7, &data);
    data = 0xCAFEBEEF55AA55AA;
    TestCase_NormalCommunicationTx(sockfd, 0xCAFEDEADBEAFCAFE, 8, &data);

    data = 0xBEEF;
    TestCase_IncompleteCommunicationTxUnaligned(sockfd, 0xCAFE, 2, &data);
    data = 0xBEEFEE;
    TestCase_NormalCommunicationTx(sockfd, 0xAACAFE, 3, &data);
    data = 0xBEEF;
    TestCase_IncompleteCommunicationTxIncomplete(sockfd, 0xCAFE, 2, &data);


    data = 0xDEADBEEF3421;
    TestCase_NormalCommunicationTx(sockfd, 0xCAFEDEADAA55, 6, &data);
    data = 0xDEADBEEF5A5A5A;
    TestCase_NormalCommunicationTx(sockfd, 0xCAFEDEAD1234, 7, &data);
    data = 0xCAFEBEEF55AA55AA;
    TestCase_NormalCommunicationTx(sockfd, 0xCAFEDEADBEAFCAFE, 8, &data);

    // close the socket
    close(sockfd);
    
    TCP_Close();


    return 0;
}
