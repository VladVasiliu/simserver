/*
 * dbg.c
 *
 *  Created on: Apr 5, 2021
 *      Author: vlad
 */

#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/syscall.h>
#include <stdio.h>
#include <errno.h>
#include <sys/reg.h>
#include <sys/user.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

#include "dbg.h"
#include "sym.h"

static pid_t pid=0;

void DBG_SetPid(pid_t child)
{
	pid = child;
}

int  DBG_SetBreakpoint(const char *Elf,const char *functionname,DBG_BreakPoint *BrkPoint)
{
	unsigned long int address=0;
	char type[64];
	unsigned int size=0;
	unsigned long data=0;
	unsigned long data_with_trap =0;
	int ret =0;
	/* ---- Check the output pointer ----*/
	if(BrkPoint == NULL) { return ret; }

	/* ---- Get the debug information ---- */
	DBG_GetSymbol(Elf,functionname,type,&size,&address);

	/* ----- Init the Breakpoint information -----*/
	BrkPoint->Name        = (char*)functionname;
	BrkPoint->address     = address;
	BrkPoint->pid         = pid;
	BrkPoint->instruction = 0;

	if(address > 0)
	{
		/* ---- Read instruction and insert a trap ----*/
		BrkPoint->instruction = ptrace(PTRACE_PEEKTEXT, BrkPoint->pid,address,NULL);
		data_with_trap = ((BrkPoint->instruction) & (~(0xFF))) | 0xCC;
		printf("INFO : Set breakpoint at %s [%lX] data %lX | %lX\n",functionname,address,data,data_with_trap);
		ptrace(PTRACE_POKETEXT, BrkPoint->pid,address,data_with_trap);
		ret = 1;
	} else 
	{
		printf("ERROR :  Cannot set the breakpoint on address NULL !\n");
	}


	return ret;
}
void DBG_StartTrace()
{
	ptrace(PTRACE_TRACEME, 0, NULL, NULL);
}

void DBG_ResetBreakpoint(DBG_BreakPoint *BrkPoint)
{
	unsigned long data_with_trap =0;
	/* ---- Check the input pointer ----*/
	if(BrkPoint == NULL) { return; }

	/* ---- Read instruction and insert a trap ----*/
	data_with_trap = ptrace(PTRACE_PEEKTEXT, pid,BrkPoint->address,NULL);

	if((data_with_trap & 0xCC) != 0xCC)
	{
		printf("ERROR : trying to clear a breakpoint at address %lX  which was not set !\n",(unsigned long)BrkPoint->address);
		return;
	}


	printf("INFO : Reset breakpoint at address %lX \n",(unsigned long)BrkPoint->address);
	if(ptrace(PTRACE_POKETEXT, pid,BrkPoint->address,BrkPoint->instruction)<0)
	{
		printf("ERROR : Error while removing the breakpoint at address %lX\n",(unsigned long)BrkPoint->address);
	}

}
void DBG_Continue()
{
	//printf("INFO : Continue Execution\n");
	if (ptrace(PTRACE_CONT, pid, 0, 0) < 0)
	{
		printf("ERROR : Error trying to continue the program\n");
	}
}
unsigned long  DBG_GetData(unsigned long  address,unsigned long size)
{
	unsigned long  data=0;


	if(size > sizeof(unsigned long))
	{
		printf("ERROR : unexpected size %lX != %lX",size,sizeof(unsigned long));
		return 0;
	}

	data = ptrace(PTRACE_PEEKDATA,pid,(void *)address,0);
	//printf(">>>[%lX] = %lX ( pid = %d %s)\n",address,data,pid,strerror(errno));
	switch(size)
	{
		case 1 : {data = data&0xFFul;}break;
		case 2 : {data = data&0xFFFFul;}break;
		case 3 : {data = data&0xFFFFFFul;}break;
		case 4 : {data = data&0xFFFFFFFFul;}break;
		case 5 : {data = data&0xFFFFFFFFFFul;}break;
		case 6 : {data = data&0xFFFFFFFFFFFFul;}break;
		case 7 : {data = data&0xFFFFFFFFFFFFFFul;}break;
		case 8 : {data = data&0xFFFFFFFFFFFFFFFFul;}break;
	}
	return data;
}
void DBG_SetData(unsigned long  address,unsigned long size,unsigned long data)
{
	unsigned long dataold = 0;
	unsigned long mask    = 0xFFFFFFFFFFFFFFFFul^(((unsigned long)1ul<<(size*8ul))-1ul);
	dataold = ptrace(PTRACE_PEEKDATA,pid,(void *)address,0);
	data    = (dataold & mask) | data;
	printf(">>>[%lX] = %lX |%lX mask %lx\n",address,dataold,data,mask);
	ptrace(PTRACE_POKEDATA,pid,(void *)address,data);
}
void DBG_WaitForBreakpoint(DBG_BreakPoint *BrkPoint)
{
	struct user_regs_struct regs;
	int wait_status=0;

	/* ---- Check the input pointer ----*/
	if(BrkPoint == NULL) { return; }

	wait(&wait_status);

	if (WIFEXITED(wait_status))
	{
		printf("INFO : Program Stopped !!!\n");
	}
	else
	{
		if (WIFSTOPPED(wait_status))
		{
			if(ptrace(PTRACE_GETREGS, pid, 0, &regs)>=0)
			{
				//printf("INFO : Process stopped at address %lX\n",(unsigned long)regs.rip);

				if(regs.rip == ((BrkPoint->address)+1))
				{
					//printf("INFO : Reset breakpoint at address %lX \n",(unsigned long)BrkPoint->address);

					regs.rip = BrkPoint->address;

					if(ptrace(PTRACE_SETREGS, pid, 0, &regs)<0)
					{
						printf("ERROR : Unable to rewind to the breakpoint location");
					}

					if(ptrace(PTRACE_POKETEXT, pid,BrkPoint->address,BrkPoint->instruction)<0)
					{
						printf("ERROR : Error while removing the breakpoint at address %lX\n",(unsigned long)BrkPoint->address);
					}
					if(ptrace(PTRACE_SINGLESTEP,pid,0,0)<0)
					{
						printf("ERROR : Error executing a single step\n");
					}
					else
					{
						unsigned long data_with_trap=0;
						wait(&wait_status);
						if (WIFEXITED(wait_status))
						{
							printf("ERROR :  Unexpected exit of the process\n");
							return ;
						}

						data_with_trap = ((BrkPoint->instruction) & (~(0xFF))) | 0xCC;
						ptrace(PTRACE_POKETEXT, BrkPoint->pid,BrkPoint->address,data_with_trap);
						//printf("INFO : Breakpoint was reached !\n");
					}
				}
			}
			else
			{
				printf("ERROR : Error reading registers !\n");
			}
		}
	}
}
